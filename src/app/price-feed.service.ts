import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PriceFeedService {

  constructor(private httpClient: HttpClient) { }

  public getPrice(ticker: string, numDays: number = 1): Observable<any> {
    return this.httpClient.get(
      environment.priceUrl + '?ticker=' + ticker + '&num_days=' + numDays
    );
  }
}
